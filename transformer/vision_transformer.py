"""
Data efficent image transformer (deit)
from https://github.com/facebookresearch/deit, https://arxiv.org/abs/2012.12877
And Vit : https://arxiv.org/abs/2010.11929
"""


from functools import partial
import math

import numpy as np
import torch
import torch.nn as nn

from ..layers import DropPath, Layer


class PatchEmbed(nn.Module):
    def __init__(self, image_shape: tuple[int, int], patch_size: int = 16,
                 in_channels: int = 3, embed_dim: int = 768):
        super().__init__()
        patch_count = (image_shape[0] // patch_size) * (image_shape[1] // patch_size)
        self.image_shape = image_shape
        self.patch_size = patch_size
        self.patch_count = patch_count

        self.projector = nn.Conv2d(in_channels, embed_dim, kernel_size=patch_size, stride=patch_size)

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        return self.projector(input_data).flatten(2).transpose(1, 2)


class SelfAttention(nn.Module):
    def __init__(self, dim: int, head_count: int, qkv_bias: bool, qk_scale: float,
                 attention_drop_rate: float, projection_drop_rate: float):
        super().__init__()
        self.head_count = head_count
        head_dim = dim // head_count
        self.scale = qk_scale or head_dim ** -0.5

        self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
        self.attention_drop = nn.Dropout(attention_drop_rate)
        self.projector = nn.Linear(dim, dim)
        self.projection_drop = nn.Dropout(projection_drop_rate)

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        batch_size, sequence_length, channel_count = input_data.shape
        qkv = self.qkv(input_data).reshape(
            batch_size, sequence_length, 3, self.head_count, channel_count // self.head_count).permute(
                2, 0, 3, 1, 4)
        # (output shape : 3, batch_size, head_count, sequence_lenght, channel_count / head_count)
        query, key, value = qkv[0], qkv[1], qkv[2]
        attention = self.attention_drop(((query @ key.transpose(-2, -1)) * self.scale).softmax(dim=-1))
        return self.projection_drop(self.projector(
            (attention @ value).transpose(1, 2).reshape(batch_size, sequence_length, channel_count)))


class Block(nn.Module):
    def __init__(self, dim: int, head_count: int, mlp_ratio: float,
                 qkv_bias: bool, qk_scale: float, drop_rate: float,
                 attention_drop_rate: float, drop_path_rate: float,
                 norm_layer=0, activation=0):
        super().__init__()

        self.norm1 = norm_layer(dim)
        self.attention = SelfAttention(dim, head_count, qkv_bias, qk_scale, attention_drop_rate, drop_rate)
        self.drop_path = DropPath(drop_path_rate) if drop_path_rate > 0.0 else nn.Identity()
        self.norm2 = norm_layer(dim)
        self.mlp = nn.Sequential(
            nn.Linear(dim, int(dim * mlp_ratio)),
            activation(),
            nn.Linear(int(dim * mlp_ratio), dim),
            nn.Dropout(drop_rate))

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        out = input_data + self.drop_path(self.attention(self.norm1(input_data)))
        return out + self.drop_path(self.mlp(self.norm2(out)))


class VissionTransformer(nn.Module):
    QK_SCALE = None
    ACTIVATION = 0
    NORM_LAYER = nn.LayerNorm

    def __init__(self, image_shape: tuple[int, int, int], class_count: int, depth: int,
                 patch_size: int = 16, embed_dim: int = 768,
                 head_count: int = 8, mlp_ratio: float = 4.0, qkv_bias: bool = True, qk_scale: float = None,
                 representation_size=None, distilled: bool = False, drop_rate: float = 0.0,
                 attention_drop_rate: float = 0.0, drop_path_rate: float = 0.0, embed_layer=PatchEmbed,
                 norm_layer=0, activation=0):
        super().__init__()
        qk_scale = qk_scale if qk_scale is not None else self.QK_SCALE
        activation = activation if activation != 0 else self.ACTIVATION
        activation = activation if activation != 0 else Layer.ACTIVATION
        norm_layer = norm_layer if norm_layer != 0 else self.NORM_LAYER

        self.class_count = class_count
        self.feature_count = self.embed_dim = embed_dim
        self.distilled = distilled
        norm_layer = norm_layer or partial(nn.LayerNorm, eps=1e-6)

        self.patch_embed = embed_layer(image_shape[1:], patch_size=patch_size,
                                       in_channels=image_shape[0], embed_dim=embed_dim)
        patch_count = self.patch_embed.patch_count
        token_count = 2 if distilled else 1

        self.class_token = nn.Parameter(torch.zeros(1, 1, embed_dim))
        self.distillation_token = nn.Parameter(torch.zeros(1, 1, embed_dim)) if distilled else None
        self.position_embeddings = nn.Parameter(torch.zeros(1, patch_count + token_count, embed_dim))
        self.position_drop = nn.Dropout(drop_rate) if drop_rate > 0.0 else nn.Identity()

        depth_path_drop_rates = np.linspace(0, drop_path_rate, depth) if drop_path_rate > 0.0 else [0.0] * depth
        self.blocks = nn.Sequential(*[
            Block(embed_dim, head_count, mlp_ratio, qkv_bias, qk_scale, drop_rate, attention_drop_rate,
                  pdr, norm_layer, activation) for pdr in depth_path_drop_rates])
        self.norm = norm_layer(embed_dim)

        # Representation Layer
        if representation_size and not distilled:
            self.feature_count = representation_size
            self.pre_logits = nn.Sequential(
                nn.Linear(embed_dim, representation_size),
                nn.Tanh())
        else:
            self.pre_logits = nn.Identity()

        # Final classifier
        self.head = nn.Linear(self.feature_count, class_count) if class_count > 0 else nn.Identity()
        self.head_distilled = nn.Linear(
            self.embed_dim, self.class_count) if class_count > 0 and distilled else nn.Identity()

        # Init weights
        nn.init.trunc_normal_(self.class_token, std=0.02)
        nn.init.trunc_normal_(self.position_embeddings, std=0.02)
        if self.distilled:
            nn.init.trunc_normal_(self.distillation_token, std=0.02)

        # Applying weights initialization made no difference so far
        self.apply(partial(self._init_weights, head_bias=-math.log(self.class_count)))

    @torch.jit.ignore
    def no_weight_decay(self) -> dict:
        return {'class_token', 'distillation_token', 'position_embeddings'}

    def get_classifier(self):
        return self.head if self.distillation_token is None else (self.head, self.head_distilled)

    def reset_classifier(self, class_count: int):
        self.class_count = class_count
        self.head = nn.Linear(self.feature_count, class_count) if class_count > 0 else nn.Identity()
        self.head_distilled = nn.Linear(
            self.embed_dim, self.class_count) if class_count > 0 and self.distilled else nn.Identity()

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        embeddings = self.patch_embed(input_data)
        class_token = self.class_token.expand(embeddings.shape[0], -1, -1)

        if self.distilled:
            block_output = self.norm(self.blocks(self.position_drop(
                torch.cat((class_token, self.distillation_token.expand(embeddings.shape[0], -1, -1), embeddings), dim=1)
                + self.position_embeddings)))
            distilled_head_output = self.head_distilled(block_output[:, 1])
            head_output = self.head(block_output[:, 0])
            if self.training and not torch.jit.is_scripting():
                return head_output, distilled_head_output
            return (head_output + distilled_head_output) / 2.0

        block_output = self.norm(self.blocks(self.position_drop(
            torch.cat((class_token, embeddings), dim=1) + self.position_embeddings)))
        return self.head(self.pre_logits(block_output[:, 0]))

    @staticmethod
    def _init_weights(module: nn.Module, name: str = '', head_bias: float = 0.0):
        if isinstance(module, nn.Linear):
            if name.startswith('head'):
                nn.init.zeros_(module.weight)
                nn.init.constant_(module.bias, head_bias)
            elif name.startswith('pre_logits'):
                nn.init.xavier_normal_(module.weight)
                nn.init.zeros_(module.bias)
        # pytorch init for conv is fine
        # elif isinstance(module, nn.Conv2d):
        #     nn.init.xavier_normal_(module.weight)
        #     if module.bias is not None:
        #         nn.init.zeros_(module.bias)
        elif isinstance(module, nn.LayerNorm):
            nn.init.ones_(module.weight)
            nn.init.zeros_(module.bias)
