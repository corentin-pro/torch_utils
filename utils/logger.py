from logging import handlers, StreamHandler
import logging
import os
import sys

from .colored_formatter import ColoredFormatter


class DummyLogger():
    def debug(self, string, *args):
        print(string, *args)

    def info(self, string, *args):
        print(string, *args)

    def warn(self, string, *args):
        print(string, *args)

    def warning(self, string, *args):
        print(string, *args)

    def error(self, string, *args):
        print(string, *args)

    def critical(self, string, *args):
        print(string, *args)

    def fatal(self, string, *args):
        print(string, *args)


def create_logger(name, log_dir, stdout=False):
    logger = logging.getLogger(name)
    os.makedirs(log_dir, exist_ok=True)
    # Adding a (rotating) file handler to the logging system : outputing in capture.log
    logger.setLevel(logging.DEBUG)
    file_log_handler = handlers.RotatingFileHandler(
        os.path.join(log_dir, name + '.log'),
        maxBytes=500000,
        backupCount=5)
    file_log_handler.setLevel(logging.DEBUG)
    log_formatter = logging.Formatter('%(asctime)s %(levelname)s : %(message)s')
    file_log_handler.setFormatter(log_formatter)
    logger.addHandler(file_log_handler)

    if stdout:
        # Adding an handler to the logging system (default has none) : outputing in stdout
        terminal_log_handler = StreamHandler(sys.stdout)
        terminal_log_handler.setLevel(logging.DEBUG)
        if os.name != 'nt':
            # Fancy color for non windows console
            colored_log_formatter = ColoredFormatter('%(asctime)s %(levelname)s : %(message)s')
            terminal_log_handler.setFormatter(colored_log_formatter)
        else:
            terminal_log_handler.setFormatter(log_formatter)
        logger.addHandler(terminal_log_handler)

    return logger
